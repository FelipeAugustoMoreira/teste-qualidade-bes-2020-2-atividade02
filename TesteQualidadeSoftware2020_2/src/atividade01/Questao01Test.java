package atividade01;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class Questao01Test {

	private static final String LINE_SEPARTOR = System.lineSeparator();

	private static  Questao01 questao = new Questao01();

	private static ByteArrayInputStream in = new ByteArrayInputStream(null);

	private static ByteArrayOutputStream out = new ByteArrayOutputStream();



	void testObterNumeroFaixa() {



		System.setIn(in);

		int numFaixa = questao.obterNumeroFaixa();
		int numFaixaEsperado = 5;
		assertEquals(numFaixa, numFaixaEsperado);

		int numFaixa1 = questao.obterNumeroFaixa();
		int numFaixaEsperado1 = 11;
		assertEquals(numFaixa1, numFaixaEsperado1);

	}


	void testVerificarPrimo() {
		Questao01 questao = new Questao01();
		
		Boolean numPrimo1 = Questao01.verificarPrimo(2);
		Boolean numPrimo2 = Questao01.verificarPrimo(5);
		Boolean numPrimo3 = Questao01.verificarPrimo(7);
		Boolean numPrimo4 = Questao01.verificarPrimo(11);
		Boolean numPrimo5 = Questao01.verificarPrimo(27);
		Boolean numPrimo6 = Questao01.verificarPrimo(89);
		Boolean numPrimo7 = Questao01.verificarPrimo(123);
		Boolean numPrimo8 = Questao01.verificarPrimo(500);
		Boolean numPrimo9 = Questao01.verificarPrimo(756);
		Boolean numPrimo10 = Questao01.verificarPrimo(999);

	}


	void testExibirPrimoNaoPrimo() {

		System.setOut(new PrintStream(out));

		questao.exibirPrimoNaoPrimo(3, true);

		String resulAtual = out.toString();
		String resulEsperado = "" + LINE_SEPARTOR;
		assertEquals(resulEsperado, resulAtual);
		
		String resulAtual1 = out.toString();
		String resulEsperado2 = "" + LINE_SEPARTOR;
		assertEquals(resulEsperado2, resulAtual1);

	}

}
